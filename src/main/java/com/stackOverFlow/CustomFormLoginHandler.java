/*
 * Copyright 2014 Red Hat, Inc.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  and Apache License v2.0 which accompanies this distribution.
 *
 *  The Eclipse Public License is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  The Apache License v2.0 is available at
 *  http://www.opensource.org/licenses/apache2.0.php
 *
 *  You may elect to redistribute this code under either of these licenses.
 */

package com.stackOverFlow;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTOptions;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.FormLoginHandler;

/**
 * @author <a href="http://tfox.org">Tim Fox</a>
 */
public class CustomFormLoginHandler implements FormLoginHandler {

  private static final Logger log = LoggerFactory.getLogger(CustomFormLoginHandler.class);

  private final AuthProvider authProvider;

  private String usernameParam;
  private String passwordParam;
  private String returnURLParam;
  private String directLoggedInOKURL;
  private JWTAuth jwt;

  @Override
  public FormLoginHandler setUsernameParam(String usernameParam) {
    this.usernameParam = usernameParam;
    return this;
  }

  @Override
  public FormLoginHandler setPasswordParam(String passwordParam) {
    this.passwordParam = passwordParam;
    return this;
  }

  @Override
  public FormLoginHandler setReturnURLParam(String returnURLParam) {
    this.returnURLParam = returnURLParam;
    return this;
  }

  @Override
  public FormLoginHandler setDirectLoggedInOKURL(String directLoggedInOKURL) {
    this.directLoggedInOKURL = directLoggedInOKURL;
    return this;
  }

  public CustomFormLoginHandler(AuthProvider authProvider, String usernameParam, String passwordParam,
                              String returnURLParam, String directLoggedInOKURL,JWTAuth jwt) {
    this.authProvider = authProvider;
    this.usernameParam = usernameParam;
    this.passwordParam = passwordParam;
    this.returnURLParam = returnURLParam;
    this.directLoggedInOKURL = directLoggedInOKURL;
    this.jwt = jwt;
  }

  @Override
  public void handle(RoutingContext context) {
    HttpServerRequest req = context.request();
    if (req.method() != HttpMethod.POST) {
      context.fail(405); // Must be a POST
    } else {
//      if (!req.isExpectMultipart()) {
//        throw new IllegalStateException("Form body not parsed - do you forget to include a BodyHandler?");
//      }
//      MultiMap params = req.formAttributes();
//      String username = params.get(usernameParam);
//      String password = params.get(passwordParam);
      System.out.println("CustomFormLoginHandler.handle() context.getBodyAsJson(): "+context.getBodyAsJson());
      JsonObject reqObject = context.getBodyAsJson();
      System.out.println("CustomFormLoginHandler.handle() reqObject: "+reqObject);
      String username = reqObject.getString(usernameParam);
      String password = reqObject.getString(passwordParam);
      String returnURLValue = reqObject.getString(returnURLParam);
      System.out.println("CustomFormLoginHandler.handleeeeeeeee() username: "+username + " password: "+password);
      if (username == null || password == null) {
        log.warn("No username or password provided in form - did you forget to include a BodyHandler?");
        context.fail(400);
      } else {
        Session session = context.session();
        System.out.println("FormLoginHandlerImpl.handle() session: "+session);
        JsonObject authInfo = new JsonObject().put("username", username).put("password", password);
        System.out.println("CustomFormLoginHandler.handle() authProvider: "+authProvider);
        authProvider.authenticate(authInfo, res -> {
        	System.out.println("CustomFormLoginHandler.handle() res.succeeded(): "+res.succeeded());
          if (res.succeeded()) {
            User user = res.result();
            System.out.println("CustomFormLoginHandler.handle() user: "+user);
            context.setUser(user);
            if (session != null) {
              // the user has upgraded from unauthenticated to authenticated
              // session should be upgraded as recommended by owasp
              session.regenerateId();
              System.out.println("CustomFormLoginHandler.handle() returnURLValue: "+returnURLValue);
              System.out.println("CustomFormLoginHandler.handle() session.get(returnURLParam): "+session.get(returnURLParam));
              if (session.get(returnURLParam) == null && returnURLValue != null) {
      		    session.put(returnURLParam, returnURLValue);
              }
              String returnURL = session.remove(returnURLParam);
              System.out.println("CustomFormLoginHandler.handle() returnURL: "+returnURL);
//              if (returnURL != null) {
                // Now redirect back to the original url
                doRedirect(req.response(), returnURL,jwt,username);
                return;
//              }
            }
            // Either no session or no return url
            if (directLoggedInOKURL != null) {
              // Redirect to the default logged in OK page - this would occur
              // if the user logged in directly at this URL without being redirected here first from another
              // url
              doRedirect(req.response(), directLoggedInOKURL,jwt,username);
            } else {
              // Just show a basic page
              req.response().end(DEFAULT_DIRECT_LOGGED_IN_OK_PAGE);
            }
          } else {
            context.fail(403);  // Failed login
          }
        });
      }
    }
  }

  private void doRedirect(HttpServerResponse response, String url,JWTAuth jwt,String username) {
	  System.out.println("CustomFormLoginHandler.doRedirect() url: "+url + " jwt: "+jwt);
//    response.putHeader("location", url).setStatusCode(302).end();
	  String token = jwt.generateToken(new JsonObject().put("username",username), new JWTOptions().setExpiresInSeconds(60000L));
	  if(url != null) {
		  response.putHeader("location", url);
	  }
	  response.putHeader("Authorization", "Bearer "+token);
	  response.putHeader("Content-Type", "text/plain");
	  System.out.println("CustomFormLoginHandler.doRedirect() HREEEEEEEEEE");
//	  response.setStatusCode(200).end(jwt.generateToken(new JsonObject().put("username",username), new JWTOptions().setExpiresInSeconds(1800L)));
	  response.setStatusCode(200).end();
  }

  private static final String DEFAULT_DIRECT_LOGGED_IN_OK_PAGE = "" +
    "<html><body><h1>Login successful</h1></body></html>";
}
